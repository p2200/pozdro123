-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 13 Gru 2021, 12:28
-- Wersja serwera: 10.4.21-MariaDB
-- Wersja PHP: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `test`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pytania`
--

CREATE TABLE `pytania` (
  `id` int(11) NOT NULL,
  `tresc_pytania` text COLLATE utf8_polish_ci NOT NULL,
  `a` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `b` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `c` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `d` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `prawidlowa_odp` varchar(250) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `pytania`
--

INSERT INTO `pytania` (`id`, `tresc_pytania`, `a`, `b`, `c`, `d`, `prawidlowa_odp`) VALUES
(1, 'Jak nazywa się metoda sortowania polegająca na podziale na n przedziałów jednakowej długości, w których\r\nnastępuje sortowanie, po czym posortowane zawartości przedziałów są poddawane analizie i prezentacji?', 'Sortowanie szybkie.', 'Sortowanie kubełkowe.', 'Sortowanie bąbelkowe.', 'Sortowanie przez wybór.', 'B'),
(2, 'Program zapisany w języku PHP ma za zadanie obliczyć średnią pozytywnych ocen ucznia od 2 do 6.\r\nWarunek wybierania ocen w pętli liczącej średnią powinien zawierać wyrażenie logiczne', '$ocena > 2 or $ocena < 6', '$ocena > 2 and $ocena < 6', '$ocena >= 2 or $ocena <= 6', '$ocena >= 2 and $ocena <= 6', 'D'),
(3, 'W języku C++ zdefiniowano zmienną: char zm1;. W jaki sposób można do niej przypisać wartość zgodnie\r\nze składnią języka?', 'zm1 = \'w\';', 'zm1 == 0x35;', 'zm1[2] = 32;', 'zm1 = \"wiadro\";', 'A'),
(4, 'W języku JavaScript, aby wydzielić fragment napisu znajdujący się pomiędzy wskazanymi przez parametr\r\nindeksami należy użyć metody', 'trim()', 'slice()', 'concat()', 'replace()', 'B'),
(0, 'W jaki sposób, stosując język PHP, zapisać w ciasteczku napis znajdujący się w zmiennej dane na czas\r\njednego dnia?', 'setcookie(\"dane\", $dane, 0);', 'setcookie(\"dane\", \"dane\", 0);', 'setcookie(\"dane\", $dane, time());', 'setcookie(\"dane\", $dane, time() + (3600*24));', 'D'),
(5, 'W jaki sposób, stosując język PHP, zapisać w ciasteczku napis znajdujący się w zmiennej dane na czas\r\njednego dnia?', 'setcookie(\"dane\", $dane, 0);', 'setcookie(\"dane\", \"dane\", 0);', 'setcookie(\"dane\", $dane, time());', 'setcookie(\"dane\", $dane, time() + (3600*24));', 'D'),
(6, 'if (empty($_POST[\"name\"])) {\r\n$nameErr = \"Name is required\";\r\n}\r\nPrzedstawiony fragment kodu PHP służy do obsługi', 'sesji.', 'ciasteczek.', 'formularza.', 'bazy danych.', 'C'),
(7, 'echo date(\"Y\");\r\nPo wykonaniu kodu PHP zostanie wyświetlona aktualna data zawierająca jedynie', 'rok.', 'dzień.', 'miesiąc i rok.', 'dzień i miesiąc.', 'A'),
(8, 'Który zapis definiuje w języku PHP komentarz wieloliniowy?', '#', '//', '/* */', '<!-- -->', 'C'),
(9, 'Który z typów relacji wymaga utworzenia tabeli pośredniej łączącej klucze główne obu tabel?', '1..1', '1..n', 'n..1', 'n..m', 'D'),
(10, 'Integralność encji w bazie danych zostanie zachowana, jeżeli między innymi', 'klucz główny będzie zawsze liczbą całkowitą.', 'każdej kolumnie zostanie przypisany typ danych.', 'dla każdej tabeli zostanie utworzony klucz główny.', 'każdy klucz główny będzie miał odpowiadający mu klucz obcy w innej tabeli.', 'C');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
